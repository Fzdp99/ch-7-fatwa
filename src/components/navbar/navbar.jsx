import {Navbar, Container, Nav, Form, Button, Offcanvas} from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';

const navbar =()=>{
  return(
    <>
      {[false, 'sm', 'md', 'lg', 'xl', 'xxl'].map((expand, i) => (
        <Navbar
          key={i}
          className="navbar"
          fixed="top"
          expand="lg"
          style={{backgroundColor: "#f1f3ff"}}
        >
          <Container className="container">
            <div className="box"></div>
            <Navbar.Toggle aria-controls={`offcanvasNavbar-expand-${expand}`} />
            <Navbar.Offcanvas
              id={`offcanvasNavbar-expand-${expand}`}
              aria-labelledby={`offcanvasNavbarLabel-expand-${expand}`}
              placement="end"
            >
              <Offcanvas.Header closeButton>
                <Offcanvas.Title id={`offcanvasNavbarLabel-expand-${expand}`}>
                  BCR
                </Offcanvas.Title>
              </Offcanvas.Header>

              <Offcanvas.Body>
                <Nav className="navigasi justify-content-end flex-grow-1 pe-3">
                  <Nav.Link style={{color:"black"}} className="hovernav me-2" href="#our">Our Services</Nav.Link>
                  <Nav.Link style={{color:"black"}} className="hovernav me-2" href="#us">Why Us</Nav.Link>
                  <Nav.Link style={{color:"black"}} className="hovernav me-2" href="#tes">Testimonial</Nav.Link>
                  <Nav.Link style={{color:"black"}} className="hovernav me-2" href="#faq">FAQ</Nav.Link>
                </Nav>
                <Form className="d-flex">
                  <Button variant="outline-success" className="tombolnav hovernav">
                    Register
                  </Button>
                </Form>
              </Offcanvas.Body>
            </Navbar.Offcanvas>
          </Container>
        </Navbar>
      ))}
    </>
  )
}

export default navbar