import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTwitter, faFacebookF, faTwitch, faInstagram } from '@fortawesome/free-brands-svg-icons'
import { faEnvelope } from '@fortawesome/free-solid-svg-icons'
import {Container,Row, Col} from 'react-bootstrap';

const Footer = ()=> {
  return(
    <>
      <Container fluid className="footer">
        <Container className="container">
          <Row className="row">
            <Col className="col colom1">
              <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
              <p>binarcarrental@gmail.com</p>
              <p>081-233-334-808</p>
            </Col>
            <Col sm={2} className="col-sm-2 colom2">
              <div className="list">
                <p>Our services</p>
                <p>Why Us</p>
                <p>Testimonial</p>
                <p>FAQ</p>
              </div>
            </Col>
            <Col className="col colom3">
              <p>Connect with us</p>
              <div className="gambar">
                <div className="icon">
                  <FontAwesomeIcon icon={faFacebookF} inverse/>
                </div>
                <div className="icon">
                  <FontAwesomeIcon icon={faInstagram} inverse/>
                </div>
                <div className="icon">
                  <FontAwesomeIcon icon={faTwitter} inverse/>
                </div>
                <div className="icon">
                  <FontAwesomeIcon icon={faEnvelope} inverse/>
                </div>
                <div className="icon">
                  <FontAwesomeIcon icon={faTwitch} inverse/>
                </div>
              </div>
            </Col>
            <Col className="col colom4">
              <div className="isi">
                <p>Copyright Binar 2022</p>
                <div className="kotak"></div>
              </div>
            </Col>
          </Row>
        </Container>
      </Container>

      <br />
      <br />
      <br />
      <br />
    </>
  )
}

export default Footer