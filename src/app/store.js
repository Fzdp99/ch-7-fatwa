import { configureStore } from "@reduxjs/toolkit";
import carsReducer from "../features/cars/reducer";

export const store = configureStore({
  reducer: {
    cars: carsReducer,
  },
});
