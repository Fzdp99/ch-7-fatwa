import "bootstrap/dist/css/bootstrap.min.css";
import Img_car from "../../assets/images/img_car.png";
import P1 from "../../assets/images/img_photo1.png";
import P2 from "../../assets/images/img_photo2.png";
import ImgS from "../../assets/images/img_service.png";
import { useNavigate } from "react-router-dom";
import { Accordion, Button, Container, Row, Col, Card } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCheck,
  faThumbsUp,
  faTag,
  faClock,
  faAward,
  faStar,
  faAngleLeft,
  faAngleRight,
} from "@fortawesome/free-solid-svg-icons";

const Home = () => {
  const history = useNavigate();
  return (
    <>
      <Container fluid className="atas" style={{ backgroundColor: "#f1f3ff" }}>
        <Container className="container">
          <Row className="row">
            <Col md={6} className="col-md-6">
              <h1 className="judul">
                Sewa & Rental Mobil Terbaik di kawasan Lamongan
              </h1>
              <p className="bawahjudul">
                Selamat datang di Binar Car Rental. Kami menyediakan mobil
                kualitas terbaik dengan harga terjangkau. Selalu siap melayani
                kebutuhanmu untuk sewa mobil selama 24 jam.
              </p>
              <Button
                onClick={() => history("/filter")}
                type="button"
                className="btn tombolsewa"
              >
                Mulai Sewa Mobil
              </Button>
            </Col>
            <Col className="col">
              <img className="gambarmobil" src={Img_car} alt="" />
            </Col>
          </Row>
        </Container>
      </Container>

      <div id="our"></div>
      <Container fluid className="ourservice">
        <Container className="container">
          <Row className="row">
            <Col className="col">
              <img className="gambarorang" src={ImgS} alt="" />
            </Col>
            <Col className="col">
              <div className="deskripsi1">
                <h1>Best Car Rental for any kind of trip in Lamongan!</h1>
                <div className="isi">
                  <p>
                    Sewa mobil di Lamongan bersama Binar Car Rental jaminan
                    harga lebih murah dibandingkan yang lain, kondisi mobil
                    baru, serta kualitas pelayanan terbaik untuk perjalanan
                    wisata, bisnis, wedding, meeting, dll.
                  </p>
                  <ul>
                    <li>
                      <div className="gambarlist">
                        <FontAwesomeIcon icon={faCheck} className="check" />
                      </div>
                      <p>Sewa Mobil Dengan Supir di Bali 12 Jam</p>
                    </li>
                    <li>
                      <div className="gambarlist">
                        <FontAwesomeIcon icon={faCheck} className="check" />
                      </div>
                      <p>Sewa Mobil Lepas Kunci di Bali 24 Jam</p>
                    </li>
                    <li>
                      <div className="gambarlist">
                        <FontAwesomeIcon icon={faCheck} className="check" />
                      </div>
                      <p>Sewa Mobil Jangka Panjang Bulanan</p>
                    </li>
                    <li>
                      <div className="gambarlist">
                        <FontAwesomeIcon icon={faCheck} className="check" />
                      </div>
                      <p>Gratis Antar - Jemput Mobil di Bandara</p>
                    </li>
                    <li>
                      <div className="gambarlist">
                        <FontAwesomeIcon icon={faCheck} className="check" />
                      </div>
                      <p>Layanan Airport Transfer / Drop In Out</p>
                    </li>
                  </ul>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </Container>

      <div id="us"></div>
      <Container fluid>
        <Container className="container whyus">
          <h1>Why Us?</h1>
          <p>Mengapa harus pilih Binar Car Rental?</p>
          <Row className="row list">
            <Col className="col">
              <Card className="card">
                <div className="gambar" style={{ backgroundColor: "#f9cc00" }}>
                  <FontAwesomeIcon icon={faThumbsUp} inverse />
                </div>
                <Card.Body className="card-body">
                  <h5 className="card-title">Mobil Lengkap</h5>
                  <p className="card-text">
                    Tersedia banyak pilihan mobil, kondisi masih baru, bersih
                    dan terawat
                  </p>
                </Card.Body>
              </Card>
            </Col>
            <Col className="col">
              <Card className="card">
                <div className="gambar" style={{ backgroundColor: "#fa2c5a" }}>
                  <FontAwesomeIcon icon={faTag} inverse />
                </div>
                <Card.Body className="card-body">
                  <h5 className="card-title">Harga Murah</h5>
                  <p className="card-text">
                    Harga murah dan bersaing, bisa bandingkan harga kami dengan
                    rental mobil lain
                  </p>
                </Card.Body>
              </Card>
            </Col>
            <Col className="col">
              <Card className="card">
                <div className="gambar" style={{ backgroundColor: "blue" }}>
                  <FontAwesomeIcon icon={faClock} inverse />
                </div>
                <Card.Body className="card-body">
                  <h5 className="card-title">Layanan 24 Jam</h5>
                  <p className="card-text">
                    Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami
                    juga tersedia di akhir minggu
                  </p>
                </Card.Body>
              </Card>
            </Col>
            <Col className="col">
              <Card className="card">
                <div className="gambar" style={{ backgroundColor: "#5cb85f" }}>
                  <FontAwesomeIcon icon={faAward} inverse />
                </div>
                <Card.Body className="card-body">
                  <h5 className="card-title">Sopir Profesional</h5>
                  <p className="card-text">
                    Sopir yang profesional, berpengalaman, jujur, ramah dan
                    selalu tepat waktu
                  </p>
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Container>
      </Container>

      <div id="tes"></div>
      <Container fluid className="testi">
        <h1>Testimonial</h1>
        <p>Berbagai review positif dari para pelanggan kami</p>
        <Container fluid>
          <div
            id="carouselExampleControls"
            className="carousel slide"
            data-bs-ride="carousel"
          >
            <div className="carousel-inner">
              <div className="carousel-item active">
                <div className="isi">
                  <img className="gambar2" src={P2} alt="" />
                  <div className="gambar1">
                    <FontAwesomeIcon icon={faStar} className="star" />
                    <FontAwesomeIcon icon={faStar} className="star" />
                    <FontAwesomeIcon icon={faStar} className="star" />
                    <FontAwesomeIcon icon={faStar} className="star" />
                    <FontAwesomeIcon icon={faStar} className="star" />
                  </div>
                  <p className="t1">
                    “Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                    sed do eiusmod lorem ipsum dolor sit amet, consectetur
                    adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet,
                    consectetur adipiscing elit, sed do eiusmod”
                  </p>
                  <p className="t2">John Dee 32, Bromo"</p>
                </div>
              </div>
              <div className="carousel-item">
                <div className="isi">
                  <img className="gambar2" src={P1} alt="" />
                  <div className="gambar1">
                    <FontAwesomeIcon icon={faStar} className="star" />
                    <FontAwesomeIcon icon={faStar} className="star" />
                    <FontAwesomeIcon icon={faStar} className="star" />
                    <FontAwesomeIcon icon={faStar} className="star" />
                    <FontAwesomeIcon icon={faStar} className="star" />
                  </div>
                  <p className="t1">
                    “Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                    sed do eiusmod lorem ipsum dolor sit amet, consectetur
                    adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet,
                    consectetur adipiscing elit, sed do eiusmod”
                  </p>
                  <p className="t2">John Dee 32, Bromo"</p>
                </div>
              </div>
            </div>
          </div>
          <div className="bt">
            <div
              className="bt1"
              type="button"
              data-bs-target="#carouselExampleControls"
              data-bs-slide="prev"
            >
              <FontAwesomeIcon icon={faAngleLeft} className="btRL" />
            </div>
            <div
              className="bt1"
              type="button"
              data-bs-target="#carouselExampleControls"
              data-bs-slide="next"
            >
              <FontAwesomeIcon icon={faAngleRight} className="btRL" />
            </div>
          </div>
        </Container>
      </Container>

      <Container fluid>
        <Container className="container">
          <div className="cta">
            <h1>Sewa Mobil di Lamongan Sekarang</h1>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua.
            </p>
            <Button
              onClick={() => history("/filter")}
              className="btn btcta"
              type="submit"
            >
              Mulai Sewa Mobil
            </Button>
          </div>
        </Container>
      </Container>

      <div id="faq"></div>
      <Container fluid>
        <Container className="container faq">
          <Row className="row">
            <Col sm={5} className="col-sm-5">
              <h1>Frequently Asked Question</h1>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
            </Col>
            <Col sm={7} className="col-sm-7">
              <Accordion id="accordionExample">
                <Accordion className="accordion">
                  <Accordion.Item eventKey="0" className="accordion-item">
                    <Accordion.Header>
                      Apa saja syarat yang di butuhkan ?
                    </Accordion.Header>
                    <Accordion.Body className="accordion-body">
                      <strong>This is the first item's accordion body.</strong>
                      It is shown by default, until the collapse plugin adds the
                      appropriate classNamees that we use to style each element.
                      These classNamees control the overall appearance, as well
                      as the showing and hiding via CSS transitions. You can
                      modify any of this with custom CSS or overriding our
                      default variables. It's also worth noting that just about
                      any HTML can go within the <code>.accordion-body</code>,
                      though the transition does limit overflow.
                    </Accordion.Body>
                  </Accordion.Item>
                </Accordion>

                <Accordion className="accordion mt-3">
                  <Accordion.Item eventKey="1" className="accordion-item">
                    <Accordion.Header>
                      Berapa hari minimal sewa mobil lepas kunci?
                    </Accordion.Header>
                    <Accordion.Body className="accordion-body">
                      <strong>This is the first item's accordion body.</strong>
                      It is shown by default, until the collapse plugin adds the
                      appropriate classNamees that we use to style each element.
                      These classNamees control the overall appearance, as well
                      as the showing and hiding via CSS transitions. You can
                      modify any of this with custom CSS or overriding our
                      default variables. It's also worth noting that just about
                      any HTML can go within the <code>.accordion-body</code>,
                      though the transition does limit overflow.
                    </Accordion.Body>
                  </Accordion.Item>
                </Accordion>

                <Accordion className="accordion mt-3">
                  <Accordion.Item eventKey="2" className="accordion-item">
                    <Accordion.Header>
                      Berapa hari sebelumnya sabaiknya booking sewa mobil?
                    </Accordion.Header>
                    <Accordion.Body className="accordion-body">
                      <strong>This is the first item's accordion body.</strong>
                      It is shown by default, until the collapse plugin adds the
                      appropriate classNamees that we use to style each element.
                      These classNamees control the overall appearance, as well
                      as the showing and hiding via CSS transitions. You can
                      modify any of this with custom CSS or overriding our
                      default variables. It's also worth noting that just about
                      any HTML can go within the <code>.accordion-body</code>,
                      though the transition does limit overflow.
                    </Accordion.Body>
                  </Accordion.Item>
                </Accordion>

                <Accordion className="accordion mt-3">
                  <Accordion.Item eventKey="3" className="accordion-item">
                    <Accordion.Header>
                      Apakah Ada biaya antar-jemput?
                    </Accordion.Header>
                    <Accordion.Body className="accordion-body">
                      <strong>This is the first item's accordion body.</strong>
                      It is shown by default, until the collapse plugin adds the
                      appropriate classNamees that we use to style each element.
                      These classNamees control the overall appearance, as well
                      as the showing and hiding via CSS transitions. You can
                      modify any of this with custom CSS or overriding our
                      default variables. It's also worth noting that just about
                      any HTML can go within the <code>.accordion-body</code>,
                      though the transition does limit overflow.
                    </Accordion.Body>
                  </Accordion.Item>
                </Accordion>

                <Accordion className="accordion mt-3">
                  <Accordion.Item eventKey="4" className="accordion-item">
                    <Accordion.Header>
                      Bagaimana jika terjadi kecelakaan ?
                    </Accordion.Header>
                    <Accordion.Body className="accordion-body">
                      <strong>This is the first item's accordion body.</strong>
                      It is shown by default, until the collapse plugin adds the
                      appropriate classNamees that we use to style each element.
                      These classNamees control the overall appearance, as well
                      as the showing and hiding via CSS transitions. You can
                      modify any of this with custom CSS or overriding our
                      default variables. It's also worth noting that just about
                      any HTML can go within the <code>.accordion-body</code>,
                      though the transition does limit overflow.
                    </Accordion.Body>
                  </Accordion.Item>
                </Accordion>
              </Accordion>
            </Col>
          </Row>
        </Container>
      </Container>
    </>
  );
};

export default Home;
