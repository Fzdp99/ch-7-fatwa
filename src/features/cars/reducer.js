import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { getCarsAPI } from "./reducerAPI";

const initialState = {
  data: [],
  status: "idle",
};

export const getCars = createAsyncThunk("cars/data", async () => {
  const response = await getCarsAPI();
  return response.data;
});

export const carsSlice = createSlice({
  name: "cars",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getCars.pending, (state) => {
        state.status = "loading";
      })
      .addCase(getCars.fulfilled, (state, action) => {
        state.status = "idle";
        state.data = action.payload;
      });
  },
});

export const {} = carsSlice.actions;

export const data = (state) => state.cars;

export default carsSlice.reducer;
