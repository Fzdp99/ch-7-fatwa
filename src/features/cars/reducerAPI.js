import axios from "axios";

export const getCarsAPI = async () => {
  const res = await axios.get(
    `https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json`
  );
  return res;
};
