// referensi : https://contactmentor.com/reactjs-filter-array-of-objects/

import React from "react";
import Img_car from "../../assets/images/img_car.png";
import { Button, Container, Row, Col } from "react-bootstrap";
import { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getCars, data } from "./reducer";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUserGroup } from "@fortawesome/free-solid-svg-icons";
import Card from "./card";

const Car = () => {
  const cars = useSelector(data);
  let newcars = cars.data;
  const dispatch = useDispatch();

  const [dataFilter, setDataFilter] = useState([]);

  const [typedriver, setTypedriver] = useState("");
  const [date, setDate] = useState("");
  const [capacity, setCapacity] = useState("");
  const [time, setTime] = useState("");

  const filterType = (data) => {
    if (typedriver == "true") {
      let newdata = data.filter((row) => row.available == true);
      return newdata;
    }
    if (typedriver == "false") {
      let newdata = data.filter((row) => row.available == false);
      return newdata;
    }
    return data;
  };

  const filterDate = (data) => {
    if(date == ""){
      return data
    }
    const newdata = data.filter((row)=>row.availableAt.slice(5,7) <= date.slice(5,7))
    return newdata
  }

  const filterCapacity = (data) => {
    if(capacity == ""){
      return data
    }
    const newdata = data.filter((row)=>row.capacity >= capacity)
    return newdata
  }

  const filterTime = (data) => {
    if(time == ""){
      return data
    }
    if(time == "Pilih Waktu"){
      return data
    }
    const newdata = data.filter((row) => {
      let tm = row.availableAt.slice(17,19)
      tm = parseInt(tm)
      return tm <= time
    })
    return newdata
  }

  const fetchData = () => {
    dispatch(getCars());
  };

  useEffect(() => {
    fetchData();
  }, []);

  useEffect(() => {
    let dtFilter = filterType(newcars);
    dtFilter = filterDate(dtFilter)
    dtFilter = filterCapacity(dtFilter)
    dtFilter = filterTime(dtFilter)
    setDataFilter(dtFilter);
  }, [typedriver,date,capacity,time]);

  return (
    <>
      <Container fluid className="atas" style={{ backgroundColor: "#f1f3ff" }}>
        <Container className="container">
          <Row className="row">
            <Col md={6} className="col-md-6">
              <h1 className="judul">
                Sewa & Rental Mobil Terbaik di kawasan Lamongan
              </h1>
              <p className="bawahjudul">
                Selamat datang di Binar Car Rental. Kami menyediakan mobil
                kualitas terbaik dengan harga terjangkau. Selalu siap melayani
                kebutuhanmu untuk sewa mobil selama 24 jam.
              </p>
                <Button type="button" className="btn tombolsewa">
                  Mulai Sewa Mobil
                </Button>
            </Col>
            <Col className="col">
              <img className="gambarmobil" src={Img_car} alt="" />
            </Col>
          </Row>
        </Container>
      </Container>

      <div className="container filter">
        <div className="kotak">
          <div className="inpt">
            <label>Type Driver</label>
            <div className="input-group">
              <select
                id="typedriver"
                className="form-select"
                // id="inputGroupSelect02"
                style={{ color: "gray", fontSize: "14px" }}
                value={typedriver}
                onChange={(e) => setTypedriver(e.target.value)}
              >
                <option value={""} style={{ color: "gray", fontSize: "14px" }} selected>
                  Pilih Type Driver
                </option>
                <option
                  value={true}
                  style={{ color: "gray", fontSize: "14px" }}
                >
                  Dengan sopir
                </option>
                <option
                  value={false}
                  style={{ color: "gray", fontSize: "14px" }}
                >
                  Tanpa Sopir (Lepas kendali)
                </option>
              </select>
            </div>
          </div>

          <div className="inpt">
            <label>Tanggal</label>
            <div className="input-group">
              <input
                id="tanggal"
                type="date"
                className="form-control"
                aria-label="Sizing example input"
                aria-describedby="inputGroup-sizing-sm"
                style={{ color: "gray", fontSize: "14px" }}
                value={date}
                onChange={(e) => setDate(e.target.value)}
              />
            </div>
          </div>

          <div className="inpt">
            <label>Waktu Jemput / Ambil</label>
            <div className="input-group">
              <select
                id="waktuambil"
                className="form-select"
                style={{ color: "gray", fontSize: "14px" }}
                value={time}
                onChange={(e) => setTime(e.target.value)}
              >
                <option value={""} style={{ color: "gray", fontSize: "14px" }} selected>
                  Pilih Waktu
                </option>
                <option value={"1"} style={{ color: "gray", fontSize: "14px" }}>
                  01:00 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                  &nbsp; &nbsp; &nbsp; &nbsp;WIB
                </option>
                <option value={"2"} style={{ color: "gray", fontSize: "14px" }}>
                  02:00 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                  &nbsp; &nbsp; &nbsp; &nbsp;WIB
                </option>
                <option value={"3"} style={{ color: "gray", fontSize: "14px" }}>
                  03:00 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                  &nbsp; &nbsp; &nbsp; &nbsp;WIB
                </option>
                <option value={"4"} style={{ color: "gray", fontSize: "14px" }}>
                  04:00 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                  &nbsp; &nbsp; &nbsp; &nbsp;WIB
                </option>
                <option value={"5"} style={{ color: "gray", fontSize: "14px" }}>
                  05:00 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                  &nbsp; &nbsp; &nbsp; &nbsp;WIB
                </option>
                <option value={"6"} style={{ color: "gray", fontSize: "14px" }}>
                  06:00 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                  &nbsp; &nbsp; &nbsp; &nbsp;WIB
                </option>
                <option value={"7"} style={{ color: "gray", fontSize: "14px" }}>
                  07:00 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                  &nbsp; &nbsp; &nbsp; &nbsp;WIB
                </option>
                <option value={"8"} style={{ color: "gray", fontSize: "14px" }}>
                  08:00 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                  &nbsp; &nbsp; &nbsp; &nbsp;WIB
                </option>
                <option value={"9"} style={{ color: "gray", fontSize: "14px" }}>
                  09:00 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                  &nbsp; &nbsp; &nbsp; &nbsp;WIB
                </option>
                <option
                  value={"10"}
                  style={{ color: "gray", fontSize: "14px" }}
                >
                  10:00 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                  &nbsp; &nbsp; &nbsp; &nbsp;WIB
                </option>
                <option
                  value={"11"}
                  style={{ color: "gray", fontSize: "14px" }}
                >
                  11:00 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                  &nbsp; &nbsp; &nbsp; &nbsp;WIB
                </option>
                <option
                  value={"12"}
                  style={{ color: "gray", fontSize: "14px" }}
                >
                  12:00 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                  &nbsp; &nbsp; &nbsp; &nbsp;WIB
                </option>
              </select>
            </div>
          </div>

          <div className="inpt">
            <label>Jumlah Penumpang (Optional)</label>
            <div className="input-group">
              <input
                id="jumlahpenumpang"
                type="number"
                className="form-control"
                aria-label="Sizing example input"
                aria-describedby="inputGroup-sizing-sm"
                placeholder="Jumlah Penumpang"
                style={{ borderRight: "0ch", fontSize: "14px" }}
                value={capacity}
                onChange={(e) => setCapacity(e.target.value)}
              />
              <label
                className="input-group-text"
                style={{ backgroundColor: "white", borderLeft: "0" }}
              >
                <FontAwesomeIcon icon={faUserGroup} />
              </label>
            </div>
          </div>

          <div className="inpt">
            <button
              id="load-btn"
              className="btn tombolnav hovernav"
              type="submit"
              style={{ height: "32px", marginTop: "20px", paddingTop: "3px" }}
            >
              Cari Mobil
            </button>
          </div>
        </div>
      </div>

      <div className="container">
        <div className="row cardCar">
          {dataFilter.map((row, i) => (
            <Card
              manufacture={row.manufacture}
              type={row.type}
              rentPerDay={row.rentPerDay}
              description={row.description}
              capacity={row.capacity}
              transmission={row.transmission}
              year={row.year}
              image={row.image}
              key={i}
            ></Card>
          ))}
        </div>
      </div>
    </>
  );
};

export default Car;
