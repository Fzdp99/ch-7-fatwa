import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faUserGroup,
  faGear,
  faCalendar,
} from "@fortawesome/free-solid-svg-icons";
const Card = (props) => {
  const {
    manufacture,
    type,
    rentPerDay,
    description,
    capacity,
    transmission,
    year,
    image,
  } = props;

  return (
    <div className="col-sm-4 mt-5">
      <div className="card">
        <img
          src="https://media.mobimoto.com/thumbs/2018/11/13/24833-rolls-royce-coupe-phantom-drophead-2016/745x489-img-24833-rolls-royce-coupe-phantom-drophead-2016.jpg"
          className="card-img-top"
          alt="..."
        />
        <div className="card-body">
          <p>
            {manufacture} / {type}
          </p>
          <h5 className="card-title">Rp {rentPerDay} / hari</h5>
          <p className="card-text">{description}</p>
          <div className="statusM">
            <div className="icon">
              <FontAwesomeIcon icon={faUserGroup} />
            </div>
            <p>{capacity} Orang</p>
          </div>
          <div className="statusM">
            <div className="icon">
              <FontAwesomeIcon icon={faGear} />
            </div>
            <p>{transmission}</p>
          </div>
          <div className="statusM">
            <div className="icon">
              <FontAwesomeIcon icon={faCalendar} />
            </div>
            <p>{year}</p>
          </div>
          <button className="btlist" type="submit">
            Pilih Mobil
          </button>
        </div>
      </div>
    </div>
  );
};

export default Card;
