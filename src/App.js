import { Routes, Route } from "react-router-dom";
import Navbar from "./components/navbar/navbar";
import Footer from "./components/footer/footer";
import Home from "./features/home/home";
import Filter from "./features/cars/car";
import "./assets/css/App.css";
import "./assets/css/style.css";
import "./assets/css/mobileR.css";
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  const link = [
    {
      path: "/",
      component: <Home />,
    },
    {
      path: "/filter",
      component: <Filter />,
    },
  ];

  return (
    <div className="App">
      <Navbar></Navbar>
      <Routes>
        {link.map((data, i) => (
          <Route key={i} path={data.path} element={data.component}></Route>
        ))}
      </Routes>
      <Footer></Footer>
    </div>
  );
}

export default App;
